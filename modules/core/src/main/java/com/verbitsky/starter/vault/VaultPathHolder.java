package com.verbitsky.starter.vault;

public interface VaultPathHolder {
    String getPath(VaultSecretType secretType);
}