package com.verbitsky.starter.exception;

public class ConfigurationException extends RuntimeException {
    private static final long serialVersionUID = 7372411704031485667L;

    public ConfigurationException(String message) {
        super(message);
    }
}
