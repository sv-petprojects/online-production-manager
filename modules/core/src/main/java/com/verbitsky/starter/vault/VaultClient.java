package com.verbitsky.starter.vault;

import java.util.Map;

public interface VaultClient<K, V> {
    Map<K, V> getValues(VaultSecretType secretType);

    String getValue(VaultSecretType secretType, String key);
}