package com.verbitsky.starter.service;

import java.util.Map;

import com.verbitsky.starter.vault.VaultSecretType;

public interface PropertyProvider<K, V> {
    Map<K,V> getPropertyMap(VaultSecretType secretType, String... keys);
}