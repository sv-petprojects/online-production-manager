package com.verbitsky.starter.service;

import org.apache.commons.lang3.ArrayUtils;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.verbitsky.starter.exception.ConfigurationException;
import com.verbitsky.starter.vault.VaultClient;
import com.verbitsky.starter.vault.VaultSecretType;

@Slf4j
@Component
public class SecretPropertyProvider implements PropertyProvider<String, String> {
    private final EnumMap<VaultSecretType, Map<String, String>> secrets;
    private final VaultClient<String, String> vaultClient;

    @Autowired
    public SecretPropertyProvider(VaultClient<String, String> vaultClient) {
        this.vaultClient = vaultClient;
        secrets = new EnumMap<>(VaultSecretType.class);
    }

    @Override
    public Map<String, String> getPropertyMap(@NonNull VaultSecretType secretType, String... keys) {
        final VaultSecretType vaultSecretType = Objects.requireNonNull(secretType);

        if (secrets.get(vaultSecretType) == null) {
            secrets.put(vaultSecretType, vaultClient.getValues(vaultSecretType));
        }

        return createRequestedPropertyMap(vaultSecretType, keys);
    }

    private Map<String, String> createRequestedPropertyMap(VaultSecretType secretType, String[] keys) {
        Map<String, String> specificSecretMap = secrets.get(secretType);
        if (ArrayUtils.isEmpty(keys)) {
            return specificSecretMap;
        }

        Map<String, String> requestedMap = Arrays.stream(keys)
                .filter(key -> {
                    if (specificSecretMap.containsKey(key)) {
                        return true;
                    } else {
                        log.warn("Received secretMap doesn't contain key: {}", key);
                        return false;
                    }
                })
                .collect(Collectors.toMap(Function.identity(), specificSecretMap::get));

        if (CollectionUtils.isEmpty(requestedMap)) {
            log.error("Received secretMap doesn't contain any requested key");
            throw new ConfigurationException("Property map is empty");
        }

        return requestedMap;
    }
}