package com.verbitsky.starter.vault;

public enum VaultSecretType {
    DATABASE("database-credentials"),
    PASSWORD_SALT("password-salt"),

    TEST_INSTANCE("test");

    private final String pathKey;

    VaultSecretType(String pathKey) {
        this.pathKey = pathKey;
    }

    public String getPathKey() {
        return pathKey;
    }
}