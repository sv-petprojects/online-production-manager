package com.verbitsky.starter.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories(basePackages = "com.verbitsky.repo")
@EntityScan(basePackages = "com.verbitsky.model")
@EnableTransactionManagement
public class JpaConfig {
}