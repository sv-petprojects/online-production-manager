package com.verbitsky.starter.vault.impl;

import org.apache.commons.lang3.StringUtils;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.vault.support.VaultResponseSupport;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.verbitsky.starter.exception.ConfigurationException;
import com.verbitsky.starter.vault.VaultClient;
import com.verbitsky.starter.vault.VaultPathHolder;
import com.verbitsky.starter.vault.VaultSecretType;

@Slf4j
@Component
public class VaultKeyValueClient implements VaultClient<String, String> {
    private final VaultTemplate vaultTemplate;
    private final VaultPathHolder pathHolder;
    private final ObjectMapper mapper;

    @Autowired
    public VaultKeyValueClient(VaultTemplate vaultTemplate, VaultPathHolder pathHolder, ObjectMapper mapper) {
        this.vaultTemplate = vaultTemplate;
        this.pathHolder = pathHolder;
        this.mapper = mapper;
    }


    @Override
    public Map<String, String> getValues(@NonNull VaultSecretType secretType) {
        final VaultSecretType vaultSecretType = Objects.requireNonNull(secretType);
        final String vaultPath = pathHolder.getPath(vaultSecretType);

        try {
            return Optional.ofNullable(vaultTemplate.read(vaultPath))
                    .map(VaultResponseSupport::getData)
                    .map(secretData -> secretData.get("data"))
                    .map(dataObject -> mapper.convertValue(dataObject, new TypeReference<Map<String, String>>() {}))
                    .filter(map -> !map.isEmpty())
                    .orElseThrow(() -> new ConfigurationException("Secrets not found: " + secretType.getPathKey()));
        } catch (Exception exception) {
            log.error("Secret read error: {}", exception.getMessage());
            throw new ConfigurationException("Secrets not found: " + secretType.getPathKey());
        }
    }

    @Override
    public String getValue(VaultSecretType secretType, String key) {
        if (StringUtils.isNotBlank(key)) {
            return Optional.ofNullable(getValues(secretType)
                    .get(key))
                    .orElseThrow(() -> {
                        log.error("Secret type={}, key={} not found", secretType.getPathKey(), key);
                        return new ConfigurationException("Secret not found: " + secretType.getPathKey());
                    });
        }

        throw new ConfigurationException("Received wrong key parameter: " + key);
    }
}