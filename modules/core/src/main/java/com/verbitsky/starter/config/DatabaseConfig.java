package com.verbitsky.starter.config;


import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

import java.util.Map;

import com.verbitsky.starter.service.PropertyProvider;
import com.verbitsky.starter.vault.VaultSecretType;
import com.zaxxer.hikari.HikariDataSource;


/*
 * DataSource configuring:
 * Getting credentials from Vault server
 * If an error occur during the credentials receiving - ConfigurationException will be thrown (fail-fast)
 * */
@Configuration
public class DatabaseConfig {
    private static final String USER_NAME = "username";
    private static final String PASSWORD = "password";

    @ConfigurationProperties("spring.datasource")
    @Bean
    @Primary
    public DataSourceProperties dataSourceProperties(PropertyProvider<String, String> propertyProvider) {
        Map<String, String> credentials =
                propertyProvider.getPropertyMap(VaultSecretType.DATABASE, USER_NAME, PASSWORD);
        DataSourceProperties dbProperties = new DataSourceProperties();
        dbProperties.setUsername(credentials.get(USER_NAME));
        dbProperties.setPassword(credentials.get(PASSWORD));
        return dbProperties;
    }

    @Bean
    public DataSource dataSource(DataSourceProperties properties) {
        return properties.initializeDataSourceBuilder()
                .type(HikariDataSource.class)
                .build();
    }

}