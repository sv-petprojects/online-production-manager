package com.verbitsky.starter.vault.impl;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import com.verbitsky.starter.exception.ConfigurationException;
import com.verbitsky.starter.vault.VaultPathHolder;
import com.verbitsky.starter.vault.VaultSecretType;

@Slf4j
@Data
@Component
@ConfigurationProperties(prefix = "vault-secrets")
public class VaultSecretPathHolder implements VaultPathHolder {
    private final Map<String, String> pathMap;

    @Override
    public String getPath(@NonNull VaultSecretType secretType) {
        final VaultSecretType vaultSecretType = Objects.requireNonNull(secretType);

        if (!pathMap.containsKey(vaultSecretType.getPathKey().toLowerCase(Locale.ROOT))) {
            log.error("VaultSecretPathHolder doesn't contain {} key", secretType.getPathKey());
            throw new ConfigurationException("Path not found, check the property file");
        }

        return pathMap.get(vaultSecretType.getPathKey().toLowerCase(Locale.ROOT));
    }
}