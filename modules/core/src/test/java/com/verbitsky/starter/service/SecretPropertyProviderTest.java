package com.verbitsky.starter.service;

import com.verbitsky.starter.exception.ConfigurationException;
import com.verbitsky.starter.vault.VaultClient;
import com.verbitsky.starter.vault.VaultSecretType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SecretPropertyProviderTest {

    @Mock
    private VaultClient<String, String> vaultClient;

    @InjectMocks
    private SecretPropertyProvider propertyProvider;

    @Test
    void getPropertyMapPositiveWithoutKeys() {
        prepareMock();
        Map<String, String> actualMap = propertyProvider.getPropertyMap(VaultSecretType.TEST_INSTANCE);

        assertFalse(actualMap.isEmpty());
        assertTrue(actualMap.containsKey("key1"));
        assertTrue(actualMap.containsKey("key2"));
        assertTrue(actualMap.containsValue("value1"));
        assertTrue(actualMap.containsValue("value2"));
    }

    @Test
    void getPropertyMapPositiveWithSpecificKey() {
        prepareMock();
        Map<String, String> actualMap = propertyProvider.getPropertyMap(VaultSecretType.TEST_INSTANCE, "key1");

        assertFalse(actualMap.isEmpty());
        assertTrue(actualMap.containsKey("key1"));
        assertTrue(actualMap.containsValue("value1"));
        assertEquals(1, actualMap.size());
    }

    @Test
    void getPropertyMapPositiveWithSpecificKeys() {
        prepareMock();
        Map<String, String> actualMap =
                propertyProvider.getPropertyMap(VaultSecretType.TEST_INSTANCE, "key1", "key3");

        assertFalse(actualMap.isEmpty());
        assertTrue(actualMap.containsKey("key1"));
        assertEquals(1, actualMap.size());
    }

    @Test
    void getPropertyMapWithNoneExistentKey() {
        prepareMock();

        ConfigurationException exception = assertThrows(
                ConfigurationException.class,
                () -> propertyProvider.getPropertyMap(VaultSecretType.TEST_INSTANCE, "nullKey"),
                "ConfigurationException expected");

        assertTrue(exception.getMessage().contains("Property map is empty"));
    }

    @Test
    void getPropertyMapWithNullArgs() {
        assertThrows(
                NullPointerException.class,
                () -> propertyProvider.getPropertyMap(null),
                "NPE expected");
    }

    private void prepareMock() {
        when(vaultClient.getValues(any())).thenReturn(
                Map.of("key1", "value1", "key2", "value2"));
    }
}