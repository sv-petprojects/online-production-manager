package com.verbitsky.starter.vault;

import com.verbitsky.starter.exception.ConfigurationException;
import com.verbitsky.starter.vault.impl.VaultSecretPathHolder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static com.verbitsky.starter.vault.VaultSecretType.DATABASE;
import static com.verbitsky.starter.vault.VaultSecretType.TEST_INSTANCE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;


class VaultSecretPathHolderTest {

    private static final String DB_PATH = "db.secret.path";
    private static VaultPathHolder pathHolder;

    @BeforeAll
    static void initPathHolder() {
        pathHolder = new VaultSecretPathHolder(Map.of(DATABASE.getPathKey(), DB_PATH));
    }

    @Test
    void getPathPositive() {
        String actualPath = pathHolder.getPath(DATABASE);

        assertNotNull(actualPath, "Expected not null path");
        assertEquals(DB_PATH, actualPath, "Expected path: db.secret.path");
    }

    @Test
    void getPathShouldThrowConfigurationException() {
        ConfigurationException exception = assertThrows(
                ConfigurationException.class,
                () -> pathHolder.getPath(TEST_INSTANCE),
                "Expected ConfigurationException");

        assertTrue(exception.getMessage().contains("Path not found"));
    }

    @Test
    void getPathShouldThrowNPE() {
        assertThrows(NullPointerException.class, () -> pathHolder.getPath(null), "Expected NPE");
    }
}