package com.verbitsky.starter.vault;

import com.verbitsky.starter.config.VaultTestConfiguration;
import com.verbitsky.starter.exception.ConfigurationException;
import com.verbitsky.starter.vault.impl.VaultKeyValueClient;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {VaultTestConfiguration.class})
class VaultKeyValueClientTest {

    @Autowired
    private VaultKeyValueClient vaultClient;

    @Test
    void getValuesPositive() {
        Map<String, String> actualSecretMap = vaultClient.getValues(VaultSecretType.DATABASE);

        assertFalse(actualSecretMap.isEmpty());
        assertTrue(actualSecretMap.containsKey("key1"));
        assertTrue(actualSecretMap.containsValue("secret1"));
    }

    @Test
    void getValuesWithNoneExistedSecrets() {
        ConfigurationException exception = assertThrows(ConfigurationException.class,
                () -> vaultClient.getValues(VaultSecretType.TEST_INSTANCE),
                "ConfigurationException expected");

        assertTrue(exception.getMessage().contains("Secrets not found"));
    }

    @Test
    void getValuesWithNullArgs() {
        assertThrows(NullPointerException.class,
                () -> vaultClient.getValues(null),
                "NPE Expected");
    }

    @Test
    void getValuePositive() {
        String actualValue = vaultClient.getValue(VaultSecretType.DATABASE, "key1");

        assertEquals("secret1", actualValue);
    }

    @Test
    void getValueWithWrongKey() {
        ConfigurationException exception = assertThrows(ConfigurationException.class,
                () -> vaultClient.getValue(VaultSecretType.DATABASE, StringUtils.EMPTY));

        assertTrue(exception.getMessage().contains("Received wrong key parameter"));
    }

    @Test
    void getValueWithNoneExistedKey() {
        ConfigurationException exception = assertThrows(ConfigurationException.class,
                () -> vaultClient.getValue(VaultSecretType.DATABASE, "none existed"));

        assertTrue(exception.getMessage().contains("Secret not found"));
    }

    @Test
    void getValueWithNoneExistedSecrets() {
        ConfigurationException exception = assertThrows(ConfigurationException.class,
                () -> vaultClient.getValue(VaultSecretType.TEST_INSTANCE, "key"),
                "ConfigurationException expected");

        assertTrue(exception.getMessage().contains("Secrets not found"));
    }
}