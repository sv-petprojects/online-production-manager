package com.verbitsky.starter.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.verbitsky.starter.vault.VaultPathHolder;
import com.verbitsky.starter.vault.VaultSecretType;
import com.verbitsky.starter.vault.impl.VaultKeyValueClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.vault.support.VaultResponse;

import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Configuration
public class VaultTestConfiguration {

    @Bean
    public VaultPathHolder vaultPathHolder() {
        VaultPathHolder vaultPathHolderMock = mock(VaultPathHolder.class);
        when(vaultPathHolderMock.getPath(VaultSecretType.DATABASE)).thenReturn("db");
        when(vaultPathHolderMock.getPath(VaultSecretType.TEST_INSTANCE)).thenReturn("test");

        return vaultPathHolderMock;
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public VaultTemplate vaultTemplate() {
        VaultTemplate vaultTemplateMock = mock(VaultTemplate.class);
        when(vaultTemplateMock.read("test")).thenReturn(generateMockVaultResponse(true));
        when(vaultTemplateMock.read("db")).thenReturn(generateMockVaultResponse(false));

        return vaultTemplateMock;
    }

    @Bean
    @Autowired
    public VaultKeyValueClient vaultClient(
            VaultTemplate vaultTemplate,
            VaultPathHolder vaultPathHolder,
            ObjectMapper objectMapper) {

        return new VaultKeyValueClient(vaultTemplate, vaultPathHolder, objectMapper);
    }

    private VaultResponse generateMockVaultResponse(boolean isEmptyResponse) {
        VaultResponse vaultResponse = new VaultResponse();
        if (!isEmptyResponse) {
            vaultResponse.setData(Map.of("data", Map.of("key1", "secret1")));
        }

        return vaultResponse;
    }
}