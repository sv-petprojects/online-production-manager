#online prodaction manager service
This service provides some functionality for company managing


#To run locally:
This application uses vault to provide credentials for database. So, to run it locally you need
to ensure that you have access to vault and sql servers. Otherwise, you can run vault and sql servers 
locally and use dev (or custom) profile to run it.
###If you don't have access to vault and sql servers:
- Install postgres (version **13** or higher: https://www.postgresql.org/download/)
- Setup your database (Execute sql script)
- Install docker (https://www.docker.com/get-started)
- Download custom vault docker image (**need to add link here**)
- run with: ***./gradlew bootRun --args='--spring.profiles.active=dev'***

###If you are granted for access to vault and sql servers:
- Check for access to vault and postgresql-server
- run with: ***./gradlew bootRun --args='--spring.profiles.active=prod'***

#Notes:
